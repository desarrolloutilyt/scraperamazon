const path = require('path');
const axios = require('axios');
const fs = require('fs');

module.exports = async url => {
	const captchaPath = path.resolve(__dirname, '../captchas', url.split('/').pop());
	const writer = fs.createWriteStream(captchaPath);

	const response = await axios({
		url,
		method: 'GET',
		responseType: 'stream',
	});

	response.data.pipe(writer);

	return new Promise((resolve, reject) => {
		writer.on('finish', () => resolve(captchaPath));
		writer.on('error', reject);
	});
};
