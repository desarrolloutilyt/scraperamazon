module.exports = {
	Codes: {
		ERROR: -1,
		EXIT: 0,
		SUCCESS: 1,
	},
	Actions: {
		EXIT: 0,
		SCRAP: 1,
	},
};
