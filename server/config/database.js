const mongoose = require('mongoose');

module.exports = async callback => {
	await mongoose.connect(process.env.DATABASE_URI, {
		user: process.env.DATABASE_USER,
		pass: process.env.DATABASE_PWD,
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	});

	if (callback) callback();
};
