const { exec } = require('child_process');

module.exports = captchaImg => {
	return new Promise((resolve, reject) => {
		exec(`python3 ./lib/captcha.py ${captchaImg}`, {}, (error, stdout, stderr) => {
			if (error) return reject(error);
			if (stderr) return reject(stderr);
			if (stdout) return resolve(stdout);
		});
	});
};
