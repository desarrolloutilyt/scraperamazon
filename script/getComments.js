module.exports = () => {
	//Función que extrae el grid de comentarios
	const getComments = () =>
		Array.from(document.querySelectorAll('#cm_cr-review_list > div[data-hook]')).map(i => ({
			rating: Number(
				i
					.querySelector('i[data-hook="review-star-rating"] > span')
					.innerText.match(/([0-9])+,\w+/g)[0]
					.replace(',', '.')
			),
			user: i.querySelector('span.a-profile-name').innerText,
			title: i.querySelector('a[data-hook="review-title"] > span').innerText,
			text: i.querySelector('span[data-hook="review-body"] > span').innerText,
		}));

	//Click para más comentarios
	const moreComments = document.querySelector('#cm_cr-pagination_bar > ul > li.a-last > a');

	return { new: getComments(), more: moreComments !== null };
};
