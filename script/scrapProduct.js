const puppeteer = require('puppeteer');
const getImage = require('./getImage');
const resolveCaptcha = require('./resolveCaptcha');
const getComments = require('./getComments');

const click = (page, selector) =>
	Promise.all([page.click(selector), page.waitForNavigation({ waitUntil: 'networkidle0' })]);

const sleep = ms => {
	return new Promise(resolve => {
		setTimeout(resolve, ms);
	});
};

module.exports = async () => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();

	//Cambiamos la resolución
	await page.setViewport({ width: 1920, height: 1080 });

	//Accedemos a la página del producto
	await page.goto(process.env.PRODUCTURL, { waitUntil: 'networkidle0' });
	//await page.screenshot({ path: './capturas/cargaInicial.png' });

	//Comprobamos si es un captcha
	const title = await page.title();
	if (title === 'Amazon CAPTCHA') {
		//Extraemos la url de la imagen
		const urlCaptcha = await page.evaluate(() => {
			return document.querySelector('div.a-row.a-text-center > img').src;
		});

		//Guardamos la imagen
		const captchaImage = await getImage(urlCaptcha);

		//Resolvemos el captcha
		const captchaText = await resolveCaptcha(captchaImage);

		//Introducimos el valor en el input
		await page.evaluate(captchaText => {
			const input = document.querySelector('#captchacharacters');
			input.value = captchaText;
		}, captchaText);

		//await page.screenshot({ path: './capturas/inputCaptcha.png' });

		//Pulsamos el botón
		await click(page, '[type="submit"]');
		await sleep(1000);
		//await page.screenshot({ path: './capturas/paginaProducto.png' });
	}

	//Navegamos a la página de los comentarios
	await click(page, '#reviews-medley-footer > div.a-row.a-spacing-large > a');
	await sleep(1000);

	//await page.screenshot({ path: './capturas/paginaProducto.png' });
	//Ejecutamos el código en la consola
	const comments = [];

	let result = await page.evaluate(getComments);

	comments.push(...result.new);

	while (result.more) {
		//Click en más comentarios
		await click(page, '#cm_cr-pagination_bar > ul > li.a-last > a');
		await sleep(1000);
		result = await page.evaluate(getComments);
		console.log(result);
		comments.push(...result.new);
	}

	console.log('COMENTARIOS', comments);

	await browser.close();
};
